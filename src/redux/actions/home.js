/* eslint-disable import/named */
import { actionTypes } from '../../constants/API';

export const checkConnect = () => (dispatch, getState, { emit }) => {
  emit(actionTypes.ping);
};

export const login = (name) => (dispatch, getState, { emit }) => {
  dispatch({ type: actionTypes.login });
  emit(actionTypes.login, name);
};

export const getData = () => (dispatch, getState, { emit }) => {
  dispatch({ type: actionTypes.subscribe_tables });
  emit(actionTypes.subscribe_tables);
};

