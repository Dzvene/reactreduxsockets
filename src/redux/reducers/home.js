/* eslint-disable*/
import { actionTypes } from '../../constants/API';

const initialState = {
  data: [],
  error: '',
  loaded: false,
  connected: false,
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.subscribe_tables:
      const data = action.payload;
      return { ...state, data};
    default:
      return state;
  }
};

export default homeReducer;
