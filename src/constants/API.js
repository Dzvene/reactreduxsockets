/* eslint-disable computed-property-spacing,fp/no-mutation,no-param-reassign */

export const uri = 'wss://js-assignment.evolutiongaming.com/ws_api';

export const actionTypes = {
  ping: 'ping',
  login: 'login',
  subscribe_tables: 'subscribe_tables',
  add_table: 'add_table',
  update_table: 'update_table',
  remove_table: 'remove_table',
};
