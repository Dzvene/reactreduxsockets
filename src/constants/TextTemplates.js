export const button = {
  confirm: 'Confirm',
  check: 'Check',
  next: 'Next',
  prev: 'Back',
  finish: 'Finish',
  newCluster: 'New',
  cancel: 'Cancel',
  save: 'Save',
  logIn: 'Log In',
  ok: 'Ok',
};

export const pagesUrl = {
  index: '/',
  login: '/login',
};
