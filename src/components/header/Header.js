import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import IconButton from 'material-ui/IconButton';

import './styles.less';

const logo = require('./../../assets/images/logo.svg'); // eslint-disable-line

class Header extends PureComponent {
  render() {
    return (
      <div className="main-header flex left">
        <div className={'flex center'}>
          <IconButton containerElement={<Link to={'/'}/>}>
            <img src={logo}/>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default Header;
