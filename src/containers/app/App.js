import { PureComponent } from 'react';
import { Switch, Route } from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';

import Header from '../../components/header';
import Home from '../../containers/pages/home';
import NotFound from '../../containers/pages/not-found';

import { pagesUrl } from '../../constants/TextTemplates';

import '../../assets/less/normalize.less';
import '../../assets/less/global.less';
import './styles.less';

const lightMuiTheme = getMuiTheme(lightBaseTheme);

class App extends PureComponent {
  render() {
    return (
      <MuiThemeProvider muiTheme={lightMuiTheme}>
        <div className={'app-wrapper flex left top'}>
          <Header/>
          <Switch>
            <Route
              component={Home}
              exact
              path={pagesUrl.index}
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
