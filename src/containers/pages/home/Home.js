/* eslint-disable */

import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { AutoSizer, Table, Column } from 'react-virtualized';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';


import { getData } from '../../../redux/actions/home';

const spinner = require('./../../../assets/images/spinner.svg'); // eslint-disable-line

import './styles.less';

const propTypes = {
  getData: PropTypes.func,
};

const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

const dataGenerate = () => {
  const data = [];

  for(let i = 0; i <= 300; i++){
    data.push({
      id: `${i}`,
      type: `type-${possible.charAt(Math.floor(Math.random() * possible.length))}`,
      name: `name-${possible.charAt(Math.floor(Math.random() * possible.length))}`,
      warning: Math.random() >= 0.8,
      players: Math.floor(Math.random() * 7),
      maxPlayers: Math.floor(Math.random() * 13),
    })
  }

  return data;
};

class Home extends PureComponent {
  state = {
    data: dataGenerate(),
    showSpinner: true,
  };

  fetchTimer;

  componentDidMount() {
    /** TODO: For getting data from api, uncomment this line **/
    // this.props.getData();

    /** TODO: Bad practice, done only for generate data **/
    this.fetchTimer = setInterval(() => {
      this.setState({
        data: dataGenerate(),
      });
    }, 2000);
  }

  componentWillReceiveProps(nextProps) {
    clearInterval(this.fetchTimer);

    if (this.state.data !== nextProps.data) {
      this.setState({
        data: nextProps.data,
      });
    }

    this.setState({
      showSpinner: false,
    });
  }

  render() {
    return (
      <div
        className={'home'}
        style={{ height: 'calc(100% - 65px)' }}
      >
        <div
          className={'content home-table'}
          style={{ height: 'calc(100% - 110px)' }}
        >
          <Table
            bodyStyle={{ height: 'calc(100% - 115px)', maxHeight: 530, overflowY: 'overlay', padding: '0 2px' }}
            className={'table in-popup table-main-header'}
            fixedHeader
            headerStyle={{ fontSize: '16px', padding: '0 2px' }}
            selectable={false}
            wrapperStyle={{
              marginBottom: -2,
            }}
          >
            <TableHeader
              adjustForCheckbox={false}
              className={'header'}
              displaySelectAll={false}
            >
              <TableRow>
                <TableHeaderColumn className={'column'}>{'Id'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Tpe'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Name'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Warning'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Players'}</TableHeaderColumn>
                <TableHeaderColumn className={'column'}>{'Max Players'}</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody
              className={'body'}
              displayRowCheckbox={false}
            >
              {this.state.data.map((row, index) => (
                <TableRow key={index} className={`${row.warning && 'warning'}`}>
                  <TableRowColumn className={'column'}>{row.id}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.type}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.name}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.warning ? 'Warning!' : 'Ok'}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.players}</TableRowColumn>
                  <TableRowColumn className={'column'}>{row.maxPlayers}</TableRowColumn>
                </TableRow>
              ))}
              {
                (this.state.data.length <= 0) &&
                <TableRow
                  colSpan="5"
                >
                  <TableHeaderColumn
                    className={'column'}
                    style={{
                      textAlign: 'center',
                    }}
                  >
                    {
                      this.state.showSpinner
                      ? <img  src={spinner} style={{ padding: 40 }} />
                      : 'No clusters installed!'
                    }
                  </TableHeaderColumn>
                </TableRow>
              }
            </TableBody>
          </Table>
        </div>
      </div>
    );
  }
}

Home.propTypes = propTypes;

const mapStateToProps = (state) => ({
  data: state.homeReducer.data,
});

const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(getData())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

