import { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';

import './styles.less';

class NotFound extends PureComponent {
  render() {
    return (
      <div
        className={'error-page'}
        style={{ height: 'calc(100% - 64)' }}
      >
        <h2>{'Page not found'}</h2>
        <RaisedButton
          backgroundColor="#f5a623"
          className={'button'}
          containerElement={<Link to={'/'}/>}
          label={'RETURN TO HOME PAGE'}
          labelStyle={{ color: '#ffffff' }}
        />
      </div>
    );
  }
}

export default NotFound;
