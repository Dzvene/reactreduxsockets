/* eslint-disable */

const webpack = require('webpack')
const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

const isProduction = process.env.NODE_ENV === 'production';

console.log(isProduction);

const prodLoader = [
    {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader',
        }),
    },
    {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: `css-loader!less-loader`,
        }),
    },
];

const devLoader = [
    {
        test: /\.less/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "less-loader" // compiles Less to CSS
        }]
    },
];

const commonLoaders = isProduction ? prodLoader : devLoader;

module.exports = {
    devtool: NODE_ENV === 'production' ? 'hidden-source-map' : 'eval-source-map',
    entry: [
        './src/index.js'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.svg/,
                loader: 'url-loader',
                query: {
                    limit: 26000,
                    mimetype: 'image/svg+xml',
                },
            },
            {
                test: /\.png/,
                loader: 'url-loader',
                query: {
                    limit: 26000,
                    mimetype: 'image/png',
                },
            },
            {
                test: /\.js/,
                use: ['babel-loader?cacheDirectory'],
                exclude: /node_modules/,
            },
            ...commonLoaders,
],
},
plugins: [
    new webpack.ProvidePlugin({
        React: 'react'
    }),
    new webpack.LoaderOptionsPlugin({
        test: /\.less$/,
        options: {
            postcss: [
                autoprefixer(
                    {
                        browsers: [
                            'last 3 version',
                            'ie >= 10',
                        ],
                    }),
            ],
        },
    }),
    new ExtractTextPlugin({
        filename: 'style.css',
        allChunks: true,
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        comments: false,
        sourceMap: true,
        minimize: false
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
        '__DEVTOOLS__': false,
        'process.env': {
            NODE_ENV: JSON.stringify(NODE_ENV),
        },
    }),
    new webpack.LoaderOptionsPlugin({ options: { postcss: [autoprefixer] } }),
    new UglifyJSPlugin({
        compress: {
            warnings: false,
        },
        exclude: /\.test\.js/i,
        sourceMap: true
    }),
    new webpack.optimize.AggressiveMergingPlugin({
        minSizeReduce: 1,
    }),
],
    devServer: {
      host: 'localhost',
      disableHostCheck: true,
      port: 3008,
      contentBase: __dirname + '/dist',
      inline: true,
      hot: false,
      historyApiFallback: true,
      proxy: [
        {
          context: ['/ws_api/**'],
          target: 'https://js-assignment.evolutiongaming.com',
          secure: false
        }
      ]
},
};
